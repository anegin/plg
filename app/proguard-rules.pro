# Default values in:
# /android-sdk/tools/proguard/proguard-android.txt

-keepattributes InnerClasses
-keepattributes Signature
-keepattributes Exceptions
-keepattributes Annotation
-keepattributes *Annotation*
-keepattributes EnclosingMethod
-renamesourcefileattribute SourceFile
-keepattributes SourceFile,LineNumberTable

-keep public class * extends java.lang.Throwable

# Android
-dontwarn android.content.**
-dontwarn android.animation.**
-dontnote android.net.http.*
-dontnote org.apache.commons.codec.**
-dontnote org.apache.http.**

-keep public class * extends android.support.design.widget.CoordinatorLayout$Behavior {
    public <init>(android.content.Context, android.util.AttributeSet);
}

# Kotlin Experimental
-keepclassmembernames class kotlinx.** {
    volatile <fields>;
}

# Gson
-keep class sun.misc.Unsafe { *; }
-keep class * implements com.google.gson.TypeAdapterFactory
-keep class * implements com.google.gson.JsonSerializer
-keep class * implements com.google.gson.JsonDeserializer

# Retrofit 2
-dontnote retrofit2.Platform
-dontwarn retrofit2.Platform$Java8
-dontwarn retrofit2.**
-keep class retrofit2.** { *; }

# OkHttp 3
-dontwarn okio.**
-dontwarn javax.annotation.Nullable
-dontwarn javax.annotation.ParametersAreNonnullByDefault
-dontwarn okhttp3.**
-keep class okhttp3.** { *; }
-keep interface okhttp3.** { *; }

# ===============================

-keep class com.texode.plg.domain.model.login.** { *; }
-keep class com.texode.plg.data.remote.model.** { *; }
