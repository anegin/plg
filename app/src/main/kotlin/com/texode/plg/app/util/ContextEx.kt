package com.texode.plg.app.util

import android.content.Context
import android.support.annotation.ColorRes
import android.support.v4.content.ContextCompat

fun Context.color(@ColorRes id: Int) = ContextCompat.getColor(this, id)

