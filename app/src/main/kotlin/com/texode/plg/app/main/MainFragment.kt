package com.texode.plg.app.main

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.texode.plg.R
import com.texode.plg.presentation.main.MainFragmentContract
import com.texode.plg.presentation.main.MainScreenContract
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_main.*
import javax.inject.Inject

class MainFragment : Fragment(), MainFragmentContract.View {

	@Inject
	override lateinit var presenter: MainFragmentContract.Presenter

	override fun onCreate(savedInstanceState: Bundle?) {
		AndroidSupportInjection.inject(this)
		super.onCreate(savedInstanceState)
	}

	override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
		return inflater.inflate(R.layout.fragment_main, container, false)
	}

	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		buttonMoveIn.setOnClickListener {
			presenter.moveIn()
		}
		buttonMoveOut.setOnClickListener {
			presenter.moveOut()
		}
	}

	override fun onStart() {
		super.onStart()
		presenter.start()
	}

	override fun onStop() {
		presenter.stop()
		super.onStop()
	}

	override fun showMoveInFragment() {
		if (activity is MainScreenContract.View) {
			(activity as MainScreenContract.View).showMoveInFragment()
		}
	}

	override fun showMoveOutFragment() {
		if (activity is MainScreenContract.View) {
			(activity as MainScreenContract.View).showMoveOutFragment()
		}
	}

}