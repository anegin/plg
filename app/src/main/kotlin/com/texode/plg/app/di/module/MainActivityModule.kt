package com.texode.plg.app.di.module

import com.texode.plg.app.di.scope.PerActivity
import com.texode.plg.app.main.MainActivity
import com.texode.plg.domain.usecase.login.GetLoginInfo
import com.texode.plg.domain.usecase.login.LogoutUser
import com.texode.plg.presentation.main.MainScreenContract
import com.texode.plg.presentation.main.MainScreenPresenter
import dagger.Module
import dagger.Provides

@Module
open class MainActivityModule {

	@Provides
	@PerActivity
	internal fun provideMainActivityView(mainActivity: MainActivity): MainScreenContract.View {
		return mainActivity
	}

	@Provides
	@PerActivity
	internal fun provideMainActivityPresenter(mainScreenView: MainScreenContract.View,
											  getLoginInfoUseCase: GetLoginInfo,
											  logoutUseCase: LogoutUser): MainScreenContract.Presenter {
		return MainScreenPresenter(mainScreenView, getLoginInfoUseCase, logoutUseCase)
	}

}