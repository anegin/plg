package com.texode.plg.app.main

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.texode.plg.R
import com.texode.plg.app.login.LoginActivity
import com.texode.plg.app.util.inTransaction
import com.texode.plg.presentation.main.MainScreenContract
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject


class MainActivity : AppCompatActivity(), MainScreenContract.View {

	@Inject
	override lateinit var presenter: MainScreenContract.Presenter

	override fun onCreate(savedInstanceState: Bundle?) {
		AndroidInjection.inject(this)
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_main)

		setSupportActionBar(toolbar)
		supportActionBar?.setDisplayShowTitleEnabled(false)
		supportActionBar?.setDisplayUseLogoEnabled(true)

		buttonLogout.setOnClickListener {
			presenter.logout()
		}
	}

	override fun onStart() {
		super.onStart()
		presenter.start()
	}

	override fun onStop() {
		presenter.stop()
		super.onStop()
	}

	override fun onSupportNavigateUp(): Boolean {
		onBackPressed()
		return true
	}

	override fun onBackPressed() {
		if (supportFragmentManager.backStackEntryCount > 0) {
			supportFragmentManager.popBackStack()
		} else {
			super.onBackPressed()
		}
	}

	override fun startLoginScreen() {
		startActivity(Intent(this, LoginActivity::class.java))
		finish()
	}

	override fun showMainFragment() {
		val fragment = supportFragmentManager.findFragmentById(R.id.layoutContent)
		if (fragment == null) {
			supportFragmentManager.inTransaction {
				add(R.id.layoutContent, MainFragment())
			}
		}
	}

	override fun showMoveInFragment() {
		supportFragmentManager.inTransaction {
			setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right)
			replace(R.id.layoutContent, MoveInFragment())
			addToBackStack(null)
		}
	}

	override fun showMoveOutFragment() {
		// todo
	}

	override fun backToPreviousFragment() {
		if (supportFragmentManager.backStackEntryCount > 0) {
			supportFragmentManager.popBackStack()
		}
	}

}