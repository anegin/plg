package com.texode.plg.app.di.module

import com.texode.plg.app.di.scope.PerActivity
import com.texode.plg.app.login.LoginActivity
import com.texode.plg.app.main.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBindingModule {

	@PerActivity
	@ContributesAndroidInjector(modules = [LoginActivityModule::class])
	abstract fun bindLoginActivity(): LoginActivity

	@PerActivity
	@ContributesAndroidInjector(modules = [MainActivityModule::class])
	abstract fun bindMainActivity(): MainActivity

}