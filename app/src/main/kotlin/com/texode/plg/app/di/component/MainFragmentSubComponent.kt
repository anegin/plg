package com.texode.plg.app.di.component

import com.texode.plg.app.main.MainFragment
import dagger.Subcomponent
import dagger.android.AndroidInjector

@Subcomponent
interface MainFragmentSubComponent : AndroidInjector<MainFragment> {

	@Subcomponent.Builder
	abstract class Builder : AndroidInjector.Builder<MainFragment>()

}