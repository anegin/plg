package com.texode.plg.app.util

import android.support.annotation.ColorRes
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat

fun Fragment.getColor(@ColorRes id: Int) = ContextCompat.getColor(requireContext(), id)

