package com.texode.plg.app.di.component

import com.texode.plg.app.main.MoveInFragment
import dagger.Subcomponent
import dagger.android.AndroidInjector

@Subcomponent
interface MoveInFragmentSubComponent : AndroidInjector<MoveInFragment> {

	@Subcomponent.Builder
	abstract class Builder : AndroidInjector.Builder<MoveInFragment>()

}