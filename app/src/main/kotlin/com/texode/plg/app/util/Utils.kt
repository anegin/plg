package com.texode.plg.app.util

object Utils {

	private const val HEX = "0123456789ABCDEF"

	fun byteArrayToHexString(byteArray: ByteArray?): String {
		if (byteArray == null) return ""
		val sb = StringBuilder(2 * byteArray.size)
		byteArray
				.map { it.toInt() }
				.forEach {
					sb.append(HEX[it.and(0xF0).shr(4)]).append(it.and(0x0F))
				}
		return sb.toString()
	}

}