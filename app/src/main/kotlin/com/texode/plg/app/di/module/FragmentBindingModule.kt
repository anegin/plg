package com.texode.plg.app.di.module

import com.texode.plg.app.di.scope.PerFragment
import com.texode.plg.app.main.MainFragment
import com.texode.plg.app.main.MoveInFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentBindingModule {

	@PerFragment
	@ContributesAndroidInjector(modules = [MainFragmentModule::class])
	abstract fun bindMainFragment(): MainFragment

	@PerFragment
	@ContributesAndroidInjector(modules = [MoveInFragmentModule::class])
	abstract fun bindMoveInFragment(): MoveInFragment

}