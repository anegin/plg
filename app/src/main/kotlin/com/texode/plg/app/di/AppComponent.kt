package com.texode.plg.app.di

import android.app.Application
import com.texode.plg.app.PlgApp
import com.texode.plg.app.di.module.ActivityBindingModule
import com.texode.plg.app.di.module.AppModule
import com.texode.plg.app.di.module.FragmentBindingModule
import com.texode.plg.app.di.scope.PerApplication
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule

@PerApplication
@Component(
		modules = [
			AndroidSupportInjectionModule::class,
			ActivityBindingModule::class,
			FragmentBindingModule::class,
			AppModule::class
		]
)
interface AppComponent {

	fun inject(app: PlgApp)

	@Component.Builder
	interface Builder {

		@BindsInstance
		fun application(application: Application): Builder

		fun build(): AppComponent

	}

}