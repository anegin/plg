package com.texode.plg.app.main

import android.app.PendingIntent
import android.content.*
import android.nfc.NfcAdapter
import android.nfc.NfcManager
import android.os.Bundle
import android.provider.Settings
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.texode.plg.R
import com.texode.plg.app.util.Utils
import com.texode.plg.app.util.getColor
import com.texode.plg.presentation.main.MainScreenContract
import com.texode.plg.presentation.main.movein.MoveInFragmentContract
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_move_in.*
import javax.inject.Inject

class MoveInFragment : Fragment(), MoveInFragmentContract.View {

	companion object {
		private val ACTION_NFC = MoveInFragment::class.java.name + ".ACTION_NFC"
	}

	@Inject
	override lateinit var presenter: MoveInFragmentContract.Presenter

	private var nfcManager: NfcManager? = null

	private val textColorPrimary: Int by lazy { getColor(R.color.text_primary) }
	private val textColorSecondary: Int by lazy { getColor(R.color.text_secondary) }

	override fun onCreate(savedInstanceState: Bundle?) {
		AndroidSupportInjection.inject(this)
		super.onCreate(savedInstanceState)
	}

	override fun onAttach(context: Context?) {
		super.onAttach(context)
		nfcManager = context?.getSystemService(Context.NFC_SERVICE) as NfcManager
	}

	override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
		return inflater.inflate(R.layout.fragment_move_in, container, false)
	}

	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		buttonBack.setOnClickListener {
			presenter.moveBack()
		}

		buttonScanCode.setOnClickListener {
			presenter.onScanNFCTagSelected()
		}
	}

	override fun onStart() {
		super.onStart()
		presenter.start()
	}

	override fun onStop() {
		presenter.stop()
		super.onStop()
	}

	override fun onResume() {
		super.onResume()
		checkButtonState()

		context?.registerReceiver(nfcTagReceiver, IntentFilter(ACTION_NFC))

		val pendingIntent = PendingIntent.getBroadcast(context, 0, Intent(ACTION_NFC), PendingIntent.FLAG_UPDATE_CURRENT)
		nfcManager?.defaultAdapter?.enableForegroundDispatch(activity, pendingIntent, null, null)
	}

	override fun onPause() {
		context?.unregisterReceiver(nfcTagReceiver)

		nfcManager?.defaultAdapter?.disableForegroundDispatch(activity)

		super.onPause()
	}

	override fun moveToPreviousFragment() {
		if (activity is MainScreenContract.View) {
			(activity as MainScreenContract.View).backToPreviousFragment()
		}
	}

	override fun isNfcAdapterPresent(): Boolean = nfcManager?.defaultAdapter != null

	override fun isNfcAdapterEnabled(): Boolean {
		val nfcAdapter = nfcManager?.defaultAdapter
		return nfcAdapter != null && nfcAdapter.isEnabled
	}

	override fun showNFCNotAvailable(showDialog: Boolean) {
		if (showDialog) {
			AlertDialog.Builder(requireContext())
					.setCancelable(true)
					.setMessage(R.string.nfc_adapter_is_not_present_on_your_device)
					.setPositiveButton(android.R.string.ok, null)
					.show()
		} else {
			textScanCode.setText(R.string.nfc_is_not_available)
			textScanCode.setTextColor(textColorSecondary)
		}
	}

	override fun showNFCDisabled(showDialog: Boolean) {
		if (showDialog) {
			AlertDialog.Builder(requireContext())
					.setCancelable(true)
					.setMessage(R.string.nfc_adapter_is_not_enabled)
					.setNegativeButton(R.string.cancel, null)
					.setPositiveButton(R.string.settings, { _, _ ->
						showSystemNFCSettings()
					})
					.show()
		} else {
			textScanCode.setText(R.string.nfc_disabled)
			textScanCode.setTextColor(textColorSecondary)
		}
	}

	override fun showNFCAvailableAndEnabled(showDialog: Boolean) {
		if (showDialog) {
			Toast.makeText(context, R.string.hold_nfc_tag, Toast.LENGTH_SHORT).show()
		} else {
			textScanCode.setText(R.string.scan_nfc_hint)
			textScanCode.setTextColor(textColorSecondary)
		}
	}

	private fun checkButtonState() {
		buttonMoveIn.isEnabled = presenter.isFormFullfilled()
	}

	private fun showSystemNFCSettings() {
		try {
			startActivity(Intent(Settings.ACTION_NFC_SETTINGS))
		} catch (e: ActivityNotFoundException) {
			e.printStackTrace()
		}
	}

	private val nfcTagReceiver = object : BroadcastReceiver() {
		override fun onReceive(context: Context?, intent: Intent?) {
			if (intent?.action == ACTION_NFC) {
				val tagId = intent.getByteArrayExtra(NfcAdapter.EXTRA_ID)
				if (tagId != null) {
					handleNfcTagId(tagId)
				}
			}
		}
	}

	private fun handleNfcTagId(tagId: ByteArray) {
		val tagIdHex = Utils.byteArrayToHexString(tagId)
		textScanCode.text = tagIdHex
		textScanCode.setTextColor(textColorPrimary)
	}

}