package com.texode.plg.app.di.module

import android.app.Application
import android.content.Context
import com.texode.plg.app.di.scope.PerApplication
import com.texode.plg.app.util.UiThread
import com.texode.plg.data.AuthRepositoryImpl
import com.texode.plg.data.BuildConfig
import com.texode.plg.data.LocalService
import com.texode.plg.data.RemoteService
import com.texode.plg.data.executor.JobExecutor
import com.texode.plg.data.local.LocalServiceImpl
import com.texode.plg.data.remote.PlgApi
import com.texode.plg.data.remote.PlgApiFactory
import com.texode.plg.data.remote.RemoteServiceImpl
import com.texode.plg.data.remote.mapper.LoginInfoMapper
import com.texode.plg.data.util.GsonHelper
import com.texode.plg.domain.base.executor.PostExecutionThread
import com.texode.plg.domain.base.executor.ThreadExecutor
import com.texode.plg.domain.repository.AuthRepository
import dagger.Module
import dagger.Provides

@Module
open class AppModule {

	@Provides
	@PerApplication
	fun provideContext(application: Application): Context = application

	@Provides
	@PerApplication
	internal fun provideThreadExecutor(): ThreadExecutor = JobExecutor()

	@Provides
	@PerApplication
	internal fun providePostExecutionThread(): PostExecutionThread = UiThread()

	@Provides
	@PerApplication
	internal fun provideGsonHelper() = GsonHelper()

	// =============

	@Provides
	@PerApplication
	internal fun provideAuthRepository(localService: LocalService, remoteService: RemoteService): AuthRepository {
		return AuthRepositoryImpl(localService, remoteService)
	}

	@Provides
	@PerApplication
	internal fun provideLocalService(context: Context, gsonHelper: GsonHelper): LocalService {
		return LocalServiceImpl(context, gsonHelper)
	}

	@Provides
	@PerApplication
	internal fun provideRemoteService(plgApi: PlgApi, loginInfoMapper: LoginInfoMapper): RemoteService {
		return RemoteServiceImpl(plgApi, loginInfoMapper)
	}

	@Provides
	@PerApplication
	internal fun providePlgApi(gsonHelper: GsonHelper): PlgApi {
		return PlgApiFactory.makePlgApi(BuildConfig.DEBUG, gsonHelper)
	}

}