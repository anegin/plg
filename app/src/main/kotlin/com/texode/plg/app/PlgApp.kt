package com.texode.plg.app

import android.app.Activity
import android.app.Application
import android.support.v4.app.Fragment
import com.jakewharton.threetenabp.AndroidThreeTen
import com.texode.plg.app.di.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject

class PlgApp : Application(), HasActivityInjector, HasSupportFragmentInjector {

	@Inject
	lateinit var activityDispatchingAndroidInjector: DispatchingAndroidInjector<Activity>

	@Inject
	lateinit var fragmentDispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

	override fun activityInjector(): AndroidInjector<Activity> = activityDispatchingAndroidInjector

	override fun supportFragmentInjector(): AndroidInjector<Fragment> = fragmentDispatchingAndroidInjector

	override fun onCreate() {
		super.onCreate()

		DaggerAppComponent
				.builder()
				.application(this)
				.build()
				.inject(this)

		AndroidThreeTen.init(this)
	}

}