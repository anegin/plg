package com.texode.plg.app.di.module

import com.texode.plg.app.di.scope.PerActivity
import com.texode.plg.app.login.LoginActivity
import com.texode.plg.domain.usecase.login.LoginUser
import com.texode.plg.presentation.login.LoginScreenContract
import com.texode.plg.presentation.login.LoginScreenPresenter
import dagger.Module
import dagger.Provides

@Module
open class LoginActivityModule {

	@Provides
	@PerActivity
	internal fun provideLoginActivityView(loginActivity: LoginActivity): LoginScreenContract.View {
		return loginActivity
	}

	@Provides
	@PerActivity
	internal fun provideLoginActivityPresenter(loginScreenView: LoginScreenContract.View, loginUserUseCase: LoginUser): LoginScreenContract.Presenter {
		return LoginScreenPresenter(loginScreenView, loginUserUseCase)
	}

}