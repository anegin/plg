package com.texode.plg.app.login

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import com.texode.plg.R
import com.texode.plg.app.main.MainActivity
import com.texode.plg.app.util.hideKeyboard
import com.texode.plg.presentation.login.LoginScreenContract
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_login.*
import javax.inject.Inject

class LoginActivity : AppCompatActivity(), LoginScreenContract.View {

	@Inject
	override lateinit var presenter: LoginScreenContract.Presenter

	private val textColorNormal: Int by lazy { getColor(R.color.text_primary) }
	private val textColorError: Int by lazy { getColor(R.color.app_red) }
	private val textColorPrimary: Int by lazy { getColor(R.color.color_primary) }

	override fun onCreate(savedInstanceState: Bundle?) {
		AndroidInjection.inject(this)
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_login)

		setResult(RESULT_CANCELED)

		editLogin.addTextChangedListener(textWatcher)
		editPassword.addTextChangedListener(textWatcher)

		editPassword.setOnEditorActionListener { _, _, _ ->
			login()
			true
		}

		buttonLogin.setOnClickListener {
			login()
		}

		checkLoginButtonState()
	}

	override fun onStart() {
		presenter.start()
		super.onStart()
	}

	override fun onStop() {
		presenter.stop()
		super.onStop()
	}

	override fun showProgress() {
		progressBar.visibility = View.VISIBLE
		buttonLogin.setTextColor(Color.TRANSPARENT)
		buttonLogin.isEnabled = false
		editLogin.isEnabled = false
		editPassword.isEnabled = false
		layoutLoginForm.isEnabled = false
		hideKeyboard()
	}

	override fun hideProgress() {
		progressBar.visibility = View.GONE
		buttonLogin.setTextColor(textColorPrimary)
		editLogin.isEnabled = true
		editPassword.isEnabled = true
		layoutLoginForm.isEnabled = true
		checkLoginButtonState()

		if (currentFocus == editLogin) {
			editLogin.setSelection(editLogin.length())
		} else if (currentFocus == editPassword) {
			editPassword.setSelection(editPassword.length())
		}
	}

	override fun onLoginSuccess() {
		startActivity(Intent(this, MainActivity::class.java))

		setResult(RESULT_OK)
		finish()
	}

	override fun onLoginError(connectionError: Boolean) {
		val message = if (connectionError) {
			R.string.error_server_connection
		} else {
			R.string.error_invalid_login_or_password
		}
		AlertDialog.Builder(this)
				.setMessage(message)
				.setPositiveButton(android.R.string.ok, null)
				.setOnDismissListener {
					editLogin.setTextColor(textColorError)
					editPassword.setTextColor(textColorError)
				}
				.show()
	}

	private fun login() {
		hideKeyboard()
		val login = editLogin.text.toString().trim()
		val password = editPassword.text.toString().trim()
		presenter.login(login, password)
	}

	private fun checkLoginButtonState() {
		val login = editLogin.text.toString().trim()
		val password = editPassword.text.toString().trim()
		buttonLogin.isEnabled = !login.isEmpty() && !password.isEmpty()
	}

	private val textWatcher = object : TextWatcher {
		override fun afterTextChanged(s: Editable?) {
		}

		override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
		}

		override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
			editLogin.setTextColor(textColorNormal)
			editPassword.setTextColor(textColorNormal)
			checkLoginButtonState()
		}
	}

}