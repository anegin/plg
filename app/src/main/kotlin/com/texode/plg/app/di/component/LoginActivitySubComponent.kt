package com.texode.plg.app.di.component

import com.texode.plg.app.login.LoginActivity
import dagger.Subcomponent
import dagger.android.AndroidInjector

@Subcomponent
interface LoginActivitySubComponent : AndroidInjector<LoginActivity> {

	@Subcomponent.Builder
	abstract class Builder : AndroidInjector.Builder<LoginActivity>()

}