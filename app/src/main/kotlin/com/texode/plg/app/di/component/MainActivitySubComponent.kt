package com.texode.plg.app.di.component

import com.texode.plg.app.main.MainActivity
import dagger.Subcomponent
import dagger.android.AndroidInjector

@Subcomponent
interface MainActivitySubComponent : AndroidInjector<MainActivity> {

	@Subcomponent.Builder
	abstract class Builder : AndroidInjector.Builder<MainActivity>()

}