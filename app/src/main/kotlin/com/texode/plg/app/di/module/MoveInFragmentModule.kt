package com.texode.plg.app.di.module

import com.texode.plg.app.di.scope.PerFragment
import com.texode.plg.app.main.MoveInFragment
import com.texode.plg.presentation.main.movein.MoveInFragmentContract
import com.texode.plg.presentation.main.movein.MoveInFragmentPresenter
import dagger.Module
import dagger.Provides

@Module
open class MoveInFragmentModule {

	@Provides
	@PerFragment
	internal fun provideMoveInFragmentView(moveInFragment: MoveInFragment): MoveInFragmentContract.View {
		return moveInFragment
	}

	@Provides
	@PerFragment
	internal fun provideMoveInFragmentPresenter(moveInFragmentView: MoveInFragmentContract.View): MoveInFragmentContract.Presenter {
		return MoveInFragmentPresenter(moveInFragmentView)
	}

}