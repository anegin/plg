package com.texode.plg.app.di.module

import com.texode.plg.app.di.scope.PerFragment
import com.texode.plg.app.main.MainFragment
import com.texode.plg.presentation.main.MainFragmentContract
import com.texode.plg.presentation.main.MainFragmentPresenter
import dagger.Module
import dagger.Provides

@Module
open class MainFragmentModule {

	@Provides
	@PerFragment
	internal fun provideMainFragmentView(mainFragment: MainFragment): MainFragmentContract.View {
		return mainFragment
	}

	@Provides
	@PerFragment
	internal fun provideMainFragmentPresenter(mainFragmentView: MainFragmentContract.View): MainFragmentContract.Presenter {
		return MainFragmentPresenter(mainFragmentView)
	}

}