package com.texode.plg.data.remote.model

import com.google.gson.annotations.SerializedName
import org.threeten.bp.LocalDateTime

/**
 * Representation for a user token model in login response fetched from the API
 */
data class UserTokenResponse(

		val id: String?,

		@SerializedName("user_id")
		val userId: Long?,

		@SerializedName("client_id")
		val clientId: Long?,

		val name: String?,

		val scopes: List<String?>?,

		val revoked: Boolean?,

		@SerializedName("created_at")
		val createdAt: LocalDateTime?,

		@SerializedName("updated_at")
		val updatedAt: LocalDateTime?,

		@SerializedName("expires_at")
		val expiresAt: LocalDateTime?

)