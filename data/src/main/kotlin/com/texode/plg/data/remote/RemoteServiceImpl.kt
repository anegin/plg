package com.texode.plg.data.remote

import com.texode.plg.data.RemoteService
import com.texode.plg.data.remote.mapper.RemoteMapper
import com.texode.plg.data.remote.model.LoginRequest
import com.texode.plg.data.remote.model.LoginResponse
import com.texode.plg.domain.model.login.LoginInfo
import com.texode.plg.domain.model.login.LoginParams
import io.reactivex.Single
import javax.inject.Inject

/**
 * Implementation for working with remote server
 */
class RemoteServiceImpl @Inject constructor(
		private val plgApi: PlgApi,
		private val loginInfoMapper: RemoteMapper<LoginResponse?, LoginInfo?>) : RemoteService {

	override fun login(loginParams: LoginParams?): Single<LoginInfo> {
		if (loginParams == null)
			throw IllegalArgumentException("LoginParams must not be null")

		return plgApi.login(LoginRequest(loginParams.email, loginParams.password))
				.map { loginResponse ->
					loginInfoMapper.mapFromRemote(loginResponse)
				}
	}

}