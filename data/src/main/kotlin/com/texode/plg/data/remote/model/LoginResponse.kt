package com.texode.plg.data.remote.model

import com.google.gson.annotations.SerializedName

/**
 * Representation for a login response fetched from the API
 */
data class LoginResponse(

		val user: UserResponse?,

		@SerializedName("access_token")
		val accessToken: String?,                 // JWT-token

		@SerializedName("expires_in")
		val expiresIn: Long?,                     // 900000

		@SerializedName("token_type")
		val tokenType: String?                    // "Bearer"

)