package com.texode.plg.data.util

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import org.threeten.bp.LocalDateTime
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class GsonHelper @Inject constructor() {

	companion object {
		private const val DATETIME_FORMAT = "yyyy-MM-dd HH:mm:ss"
	}

	val gson: Gson = GsonBuilder()
			.registerTypeAdapter(LocalDateTime::class.java, LocalDateTimeConverter(DATETIME_FORMAT))
			.setLenient()
			.create()

	fun toJson(src: Any?): String {
		return gson.toJson(src)
	}

	fun <T> fromJson(src: String?): T {
		// todo something wrong here
		return gson.fromJson<T>(src, object : TypeToken<T>() {}.type)
	}

}