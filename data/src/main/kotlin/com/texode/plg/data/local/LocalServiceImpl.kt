package com.texode.plg.data.local

import android.content.Context
import com.google.gson.JsonSyntaxException
import com.texode.plg.data.LocalService
import com.texode.plg.data.local.model.LoginRequiredException
import com.texode.plg.data.util.GsonHelper
import com.texode.plg.domain.model.login.LoginInfo
import io.reactivex.Single
import java.io.File
import javax.inject.Inject

class LocalServiceImpl @Inject constructor(
		private val context: Context,
		private val gsonHelper: GsonHelper) : LocalService {

	private var loginInfo: LoginInfo? = null

	override fun getLoginInfo(): Single<LoginInfo> {
		return Single.defer<LoginInfo> {
			if (loginInfo == null) {
				loginInfo = loadLoginInfoFromFile()
			}
			if (loginInfo != null) {
				Single.just<LoginInfo>(loginInfo)
			} else {
				Single.error(LoginRequiredException())
			}
		}
	}

	override fun saveLoginInfo(loginInfo: LoginInfo) {
		this.loginInfo = loginInfo
		saveLoginInfoToFile(loginInfo)
	}

	override fun clearLoginInfo() {
		this.loginInfo = null
		removeLoginInfoFile()
	}

	private fun saveLoginInfoToFile(loginInfo: LoginInfo?) {
		val loginInfoJson = gsonHelper.gson.toJson(loginInfo)
		getLoginInfoFile().printWriter().use { out ->
			out.write(loginInfoJson)
		}
	}

	private fun loadLoginInfoFromFile(): LoginInfo? {
		val loginInfoJson = getLoginInfoFile().bufferedReader().use {
			it.readText()
		}
		return try {
			gsonHelper.gson.fromJson<LoginInfo>(loginInfoJson, LoginInfo::class.java)
		} catch (e: JsonSyntaxException) {
			null
		}
	}

	private fun removeLoginInfoFile() {
		getLoginInfoFile().delete()
	}

	private fun getLoginInfoFile(): File {
		return File(context.filesDir, "login_info")
	}

}