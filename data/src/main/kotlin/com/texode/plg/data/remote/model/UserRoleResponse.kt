package com.texode.plg.data.remote.model

import com.google.gson.annotations.SerializedName
import org.threeten.bp.LocalDateTime

/**
 * Representation for a user role model in login response fetched from the API
 */
data class UserRoleResponse(

		val id: Long?,

		val tag: String?,

		val name: String?,

		val scopes: List<String?>?,

		@SerializedName("created_at")
		val createdAt: LocalDateTime?,

		@SerializedName("updated_at")
		val updatedAt: LocalDateTime?

)
