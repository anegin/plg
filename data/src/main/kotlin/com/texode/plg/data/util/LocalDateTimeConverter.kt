package com.texode.plg.data.util

import com.google.gson.*
import org.threeten.bp.LocalDateTime
import org.threeten.bp.format.DateTimeFormatter
import java.lang.reflect.Type
import java.text.ParseException

class LocalDateTimeConverter(pattern: String) : JsonSerializer<LocalDateTime>, JsonDeserializer<LocalDateTime> {

	private val dateTimeFormat = DateTimeFormatter.ofPattern(pattern)

	override fun serialize(src: LocalDateTime?, typeOfSrc: Type?, context: JsonSerializationContext?): JsonElement {
		return if (src == null) {
			JsonNull.INSTANCE
		} else {
			JsonPrimitive(dateTimeFormat.format(src))
		}
	}

	@Throws(ParseException::class)
	override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): LocalDateTime? {
		return if (json == null) {
			null
		} else {
			dateTimeFormat.parse(json.asString, LocalDateTime::from)
		}
	}

}