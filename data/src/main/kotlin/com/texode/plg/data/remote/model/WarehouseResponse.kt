package com.texode.plg.data.remote.model

data class WarehouseResponse(

		val id: Long?,            // Id of the warehouse

		val name: String?         // Name of the warehouse

)