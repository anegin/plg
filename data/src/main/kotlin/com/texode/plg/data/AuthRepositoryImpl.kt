package com.texode.plg.data

import com.texode.plg.domain.model.login.LoginInfo
import com.texode.plg.domain.model.login.LoginParams
import com.texode.plg.domain.repository.AuthRepository
import io.reactivex.Completable
import io.reactivex.Single
import javax.inject.Inject

class AuthRepositoryImpl @Inject constructor(
		private val localService: LocalService,
		private val remoteService: RemoteService) : AuthRepository {

	override fun getLoginInfo(): Single<LoginInfo> {
		return localService.getLoginInfo()
	}

	override fun login(loginParams: LoginParams?): Single<LoginInfo> {
		return remoteService.login(loginParams)
				.doOnSuccess {
					localService.saveLoginInfo(it)
				}
	}

	override fun logout(): Completable {
		return Completable.defer {
			localService.clearLoginInfo()
			Completable.complete()
		}
	}

}