package com.texode.plg.data.remote.model

data class CustomerResponse(

		val id: Long?,            // Id of the customer

		val name: String?         // Name of the customer

)