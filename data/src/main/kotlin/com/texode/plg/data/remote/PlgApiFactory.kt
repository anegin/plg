package com.texode.plg.data.remote

import com.google.gson.Gson
import com.texode.plg.data.util.GsonHelper
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

/**
 * Provide "make" methods to create instances of [PlgApi]
 * and its related dependencies, such as OkHttpClient, Gson, etc.
 */
object PlgApiFactory {

	private const val API_URL = "http://plg.dock.debugged.fi/"

	fun makePlgApi(isDebug: Boolean, gsonHelper: GsonHelper): PlgApi {
		val okHttpClient = makeOkHttpClient(
				makeLoggingInterceptor(isDebug))
		return makePlgApi(okHttpClient, gsonHelper.gson)
	}

	private fun makePlgApi(okHttpClient: OkHttpClient, gson: Gson): PlgApi {
		val retrofit = Retrofit.Builder()
				.baseUrl(API_URL)
				.client(okHttpClient)
				.addCallAdapterFactory(RxJava2CallAdapterFactory.create())
				.addConverterFactory(GsonConverterFactory.create(gson))
				.build()
		return retrofit.create(PlgApi::class.java)
	}

	private fun makeOkHttpClient(httpLoggingInterceptor: HttpLoggingInterceptor): OkHttpClient {
		return OkHttpClient.Builder()
				.addInterceptor(httpLoggingInterceptor)
				.connectTimeout(5, TimeUnit.SECONDS)
				.readTimeout(5, TimeUnit.SECONDS)
				.build()
	}

	private fun makeLoggingInterceptor(isDebug: Boolean): HttpLoggingInterceptor {
		val logging = HttpLoggingInterceptor()
		logging.level = if (isDebug)
			HttpLoggingInterceptor.Level.BODY
		else
			HttpLoggingInterceptor.Level.NONE
		return logging
	}

}