package com.texode.plg.data.remote

import com.texode.plg.data.remote.model.CustomerResponse
import com.texode.plg.data.remote.model.LoginRequest
import com.texode.plg.data.remote.model.LoginResponse
import com.texode.plg.data.remote.model.WarehouseResponse
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

/**
 * Defines the abstract methods used for interacting with the server API
 */
interface PlgApi {

	/**
	 * Authentication - Login
	 * HTTP 200 OK - Success
	 * HTTP 400 Bad Request - invalid login (or invalid request) - {}
	 * HTTP 401 Unathorized - invalid password - {"error":"invalid_credentials","message":"The user credentials were incorrect."}
	 */
	@POST("api/auth/login")
	fun login(@Body loginRequest: LoginRequest): Single<LoginResponse>

	/**
	 * Customers - Search customers
	 * Authorized request
	 *
	 * HTTP 200 OK - Success
	 * HTTP 404 Not found - invalid auth-token (???)
	 */
	@GET("api/warehouse/customers")
	fun getCustomers(): Single<List<CustomerResponse>>

	/**
	 * Warehouses - Get all warehouses
	 * Authorized request
	 *
	 * HTTP 200 OK - Success
	 * HTTP 404 Not found - invalid auth-token (???)
	 */
	@GET("api/warehouse/warehouses")
	fun getWarehouses(): Single<List<WarehouseResponse>>

}
