package com.texode.plg.data.remote.mapper

import com.texode.plg.data.remote.model.CustomerResponse
import com.texode.plg.domain.model.Customer
import com.texode.plg.domain.model.login.LoginInfo
import javax.inject.Inject

/**
 * Map a [CustomerResponse] to a [LoginInfo] instance when data is moving between
 * this REMOTE and the DATA layer
 */
open class CustomerMapper @Inject constructor() : RemoteMapper<CustomerResponse?, Customer?> {

	override fun mapFromRemote(src: CustomerResponse?): Customer? {
		if (src == null) return null
		return Customer(src.id, src.name)
	}

}