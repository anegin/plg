package com.texode.plg.data

import com.texode.plg.domain.model.login.LoginInfo
import com.texode.plg.domain.model.login.LoginParams
import com.texode.plg.domain.model.login.User
import io.reactivex.Single

interface RemoteService {

	fun login(loginParams: LoginParams?): Single<LoginInfo>

}