package com.texode.plg.data.remote.mapper

/**
 * Interface for model mappers. It provides helper methods that facilitate
 * retrieving of models from remote data source
 *
 * @param <M> the remote model input type
 * @param <E> the local model output type
 */
interface RemoteMapper<in R, out L> {

	fun mapFromRemote(src: R?): L?

}