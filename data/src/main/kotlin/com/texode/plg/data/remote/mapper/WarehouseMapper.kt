package com.texode.plg.data.remote.mapper

import com.texode.plg.data.remote.model.WarehouseResponse
import com.texode.plg.domain.model.Warehouse
import javax.inject.Inject

/**
 * Map a [WarehouseResponse] to a [Warehouse] instance when data is moving between
 * this REMOTE and the DATA layer
 */
open class WarehouseMapper @Inject constructor() : RemoteMapper<WarehouseResponse?, Warehouse?> {

	override fun mapFromRemote(src: WarehouseResponse?): Warehouse? {
		if (src == null) return null
		return Warehouse(src.id, src.name)
	}

}