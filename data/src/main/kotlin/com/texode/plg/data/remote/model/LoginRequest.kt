package com.texode.plg.data.remote.model

/**
 * Representation for a login request sent to the API login method
 */
data class LoginRequest(

		val email: String,

		val password: String

)