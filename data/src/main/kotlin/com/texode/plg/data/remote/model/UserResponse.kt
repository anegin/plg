package com.texode.plg.data.remote.model

import com.google.gson.annotations.SerializedName
import org.threeten.bp.LocalDateTime

/**
 * Representation for a user model in login response fetched from the API
 */
data class UserResponse(

		val id: Long?,

		val name: String?,

		val email: String?,

		@SerializedName("refresh_token")
		val refreshToken: String?,

		@SerializedName("created_at")
		val createdAt: LocalDateTime?,

		@SerializedName("updated_at")
		val updatedAt: LocalDateTime?,

		@SerializedName("role_id")
		val roleId: Long?,

		val tokens: List<UserTokenResponse?>?,

		val role: UserRoleResponse?

)