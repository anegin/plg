package com.texode.plg.data

import com.texode.plg.domain.model.login.LoginInfo
import io.reactivex.Single

interface LocalService {

	fun getLoginInfo(): Single<LoginInfo>

	fun saveLoginInfo(loginInfo: LoginInfo)

	fun clearLoginInfo()

}