package com.texode.plg.data.remote.mapper

import com.texode.plg.data.remote.model.LoginResponse
import com.texode.plg.data.remote.model.UserResponse
import com.texode.plg.data.remote.model.UserRoleResponse
import com.texode.plg.data.remote.model.UserTokenResponse
import com.texode.plg.domain.model.login.LoginInfo
import com.texode.plg.domain.model.login.User
import com.texode.plg.domain.model.login.UserRole
import com.texode.plg.domain.model.login.UserToken
import javax.inject.Inject

/**
 * Map a [LoginResponse] to a [LoginInfo] instance when data is moving between
 * this REMOTE and the DATA layer
 */
open class LoginInfoMapper @Inject constructor() : RemoteMapper<LoginResponse?, LoginInfo?> {

	override fun mapFromRemote(src: LoginResponse?): LoginInfo? {
		if (src == null) return null
		return LoginInfo(
				mapUserFromUserResponse(src.user),
				src.accessToken,
				src.expiresIn,
				src.tokenType)
	}

	private fun mapUserFromUserResponse(src: UserResponse?): User? {
		if (src == null) return null
		return User(
				src.id,
				src.name,
				src.email,
				src.refreshToken,
				src.createdAt,
				src.updatedAt,
				src.roleId,
				src.tokens?.map {
					mapUserTokenFromUserTokenResponse(it)
				},
				mapUserRoleFromUserRoleResponse(src.role))
	}

	private fun mapUserTokenFromUserTokenResponse(src: UserTokenResponse?): UserToken? {
		if (src == null) return null
		return UserToken(
				src.id,
				src.userId,
				src.clientId,
				src.name,
				src.scopes,
				src.revoked,
				src.createdAt,
				src.updatedAt,
				src.expiresAt)
	}

	private fun mapUserRoleFromUserRoleResponse(src: UserRoleResponse?): UserRole? {
		if (src == null) return null
		return UserRole(
				src.id,
				src.tag,
				src.name,
				src.scopes,
				src.createdAt,
				src.updatedAt)
	}

}