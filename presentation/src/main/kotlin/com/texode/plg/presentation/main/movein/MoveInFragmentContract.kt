package com.texode.plg.presentation.main.movein

import com.texode.plg.presentation.BasePresenter
import com.texode.plg.presentation.BaseView

interface MoveInFragmentContract {

	interface View : BaseView<Presenter> {

		fun moveToPreviousFragment()

		fun isNfcAdapterPresent(): Boolean

		fun isNfcAdapterEnabled(): Boolean

		fun showNFCNotAvailable(showDialog: Boolean)

		fun showNFCDisabled(showDialog: Boolean)

		fun showNFCAvailableAndEnabled(showDialog: Boolean)

	}

	interface Presenter : BasePresenter {

		fun isFormFullfilled(): Boolean

		fun moveBack()

		fun onScanNFCTagSelected()

	}

}