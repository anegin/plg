package com.texode.plg.presentation.main

import com.texode.plg.domain.base.interactor.CompletableUseCase
import com.texode.plg.domain.base.interactor.SingleUseCase
import com.texode.plg.domain.model.login.LoginInfo
import io.reactivex.observers.DisposableCompletableObserver
import io.reactivex.observers.DisposableSingleObserver
import javax.inject.Inject

class MainScreenPresenter @Inject constructor(
		val mainScreenView: MainScreenContract.View,
		val getLoginInfoUseCase: SingleUseCase<LoginInfo, Nothing?>,
		val logoutUseCase: CompletableUseCase<Nothing?>) : MainScreenContract.Presenter {

	init {
		mainScreenView.presenter = this
	}

	override fun start() {
		ensureUserAuthorized()
	}

	override fun stop() {
		getLoginInfoUseCase.unsubscribe()
		logoutUseCase.unsubscribe()
	}

	// ==================

	private fun ensureUserAuthorized() {
		getLoginInfoUseCase.execute(GetLoginInfoSubscriber())
	}

	inner class GetLoginInfoSubscriber : DisposableSingleObserver<LoginInfo>() {
		override fun onSuccess(loginInfo: LoginInfo) {
			mainScreenView.showMainFragment()
		}

		override fun onError(e: Throwable) {
			mainScreenView.startLoginScreen()
		}
	}

	// ==================

	override fun logout() {
		logoutUseCase.execute(LogoutSubscriber())
	}

	inner class LogoutSubscriber : DisposableCompletableObserver() {
		override fun onComplete() {
			mainScreenView.startLoginScreen()
		}

		override fun onError(e: Throwable) {
			mainScreenView.startLoginScreen()
		}
	}

}