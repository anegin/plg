package com.texode.plg.presentation.main

import com.texode.plg.presentation.BasePresenter
import com.texode.plg.presentation.BaseView

interface MainFragmentContract {

	interface View : BaseView<Presenter> {

		fun showMoveInFragment()

		fun showMoveOutFragment()

	}

	interface Presenter : BasePresenter {

		fun moveIn()

		fun moveOut()

	}

}