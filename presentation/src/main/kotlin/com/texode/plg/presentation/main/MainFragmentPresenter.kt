package com.texode.plg.presentation.main

import javax.inject.Inject

class MainFragmentPresenter @Inject constructor(
		val mainFragmentView: MainFragmentContract.View) : MainFragmentContract.Presenter {

	init {
		mainFragmentView.presenter = this
	}

	override fun start() {
	}

	override fun stop() {
	}

	override fun moveIn() {
		mainFragmentView.showMoveInFragment()
	}

	override fun moveOut() {
		mainFragmentView.showMoveOutFragment()
	}

}