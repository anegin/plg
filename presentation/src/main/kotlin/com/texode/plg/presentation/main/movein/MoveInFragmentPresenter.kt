package com.texode.plg.presentation.main.movein

import javax.inject.Inject

class MoveInFragmentPresenter @Inject constructor(
		val moveInFragmentView: MoveInFragmentContract.View) : MoveInFragmentContract.Presenter {

	init {
		moveInFragmentView.presenter = this
	}

	override fun start() {
		checkNfcAvailability(false)
	}

	override fun stop() {
	}

	override fun moveBack() {
		moveInFragmentView.moveToPreviousFragment()
	}

	override fun onScanNFCTagSelected() {
		checkNfcAvailability(true)
	}

	override fun isFormFullfilled(): Boolean {
		return true
	}

	private fun checkNfcAvailability(showDialog: Boolean) {
		if (!moveInFragmentView.isNfcAdapterPresent()) {
			moveInFragmentView.showNFCNotAvailable(showDialog)
		} else if (!moveInFragmentView.isNfcAdapterEnabled()) {
			moveInFragmentView.showNFCDisabled(showDialog)
		} else {
			moveInFragmentView.showNFCAvailableAndEnabled(showDialog)
		}
	}

}