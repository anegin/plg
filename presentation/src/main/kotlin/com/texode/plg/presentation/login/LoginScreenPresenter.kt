package com.texode.plg.presentation.login

import com.texode.plg.domain.base.interactor.SingleUseCase
import com.texode.plg.domain.model.login.LoginInfo
import com.texode.plg.domain.model.login.LoginParams
import io.reactivex.observers.DisposableSingleObserver
import java.net.UnknownHostException
import javax.inject.Inject

class LoginScreenPresenter @Inject constructor(
		val loginScreenView: LoginScreenContract.View,
		val loginUseCase: SingleUseCase<LoginInfo, LoginParams?>) : LoginScreenContract.Presenter {

	init {
		loginScreenView.presenter = this
	}

	override fun start() {
	}

	override fun stop() {
		loginUseCase.unsubscribe()
	}

	override fun login(login: String, password: String) {
		loginScreenView.showProgress()
		loginUseCase.execute(LoginSubscriber(), LoginParams(login, password))
	}

	inner class LoginSubscriber : DisposableSingleObserver<LoginInfo>() {
		override fun onSuccess(loginInfo: LoginInfo) {
			loginScreenView.onLoginSuccess()
		}

		override fun onError(e: Throwable) {
			loginScreenView.hideProgress()
			loginScreenView.onLoginError(e is UnknownHostException)

			// e: HTTPException when
			//  - HTTP 401 Unathorized (invalid password)
			//  - HTTP 400 Bad Request (invalid login or bad request)

			// e: UnknownHostException when there is no network connection or server is not available)
		}
	}

}