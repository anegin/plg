package com.texode.plg.presentation.login

import com.texode.plg.presentation.BasePresenter
import com.texode.plg.presentation.BaseView

interface LoginScreenContract {

	interface View : BaseView<Presenter> {

		fun showProgress()

		fun hideProgress()

		fun onLoginSuccess()

		fun onLoginError(connectionError: Boolean)

	}

	interface Presenter : BasePresenter {

		fun login(login: String, password: String)

	}

}