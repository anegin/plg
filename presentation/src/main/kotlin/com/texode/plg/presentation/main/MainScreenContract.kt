package com.texode.plg.presentation.main

import com.texode.plg.presentation.BasePresenter
import com.texode.plg.presentation.BaseView

interface MainScreenContract {

	interface View : BaseView<Presenter> {

		fun startLoginScreen()

		fun showMainFragment()

		fun showMoveInFragment()

		fun showMoveOutFragment()

		fun backToPreviousFragment()

	}

	interface Presenter : BasePresenter {

		fun logout()

	}

}