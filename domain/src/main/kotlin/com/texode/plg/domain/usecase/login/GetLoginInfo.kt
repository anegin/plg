package com.texode.plg.domain.usecase.login

import com.texode.plg.domain.base.executor.PostExecutionThread
import com.texode.plg.domain.base.executor.ThreadExecutor
import com.texode.plg.domain.base.interactor.SingleUseCase
import com.texode.plg.domain.model.login.LoginInfo
import com.texode.plg.domain.repository.AuthRepository
import io.reactivex.Single
import javax.inject.Inject

class GetLoginInfo @Inject constructor(
		private val authRepository: AuthRepository,
		threadExecutor: ThreadExecutor,
		postExecutionThread: PostExecutionThread) : SingleUseCase<LoginInfo, Nothing?>(threadExecutor, postExecutionThread) {

	override fun buildUseCaseObservable(params: Nothing?): Single<LoginInfo> {
		return authRepository.getLoginInfo()
	}

}