package com.texode.plg.domain.repository

import com.texode.plg.domain.model.login.LoginInfo
import com.texode.plg.domain.model.login.LoginParams
import io.reactivex.Completable
import io.reactivex.Single

interface AuthRepository {

	fun getLoginInfo(): Single<LoginInfo>

	fun login(loginParams: LoginParams?): Single<LoginInfo>

	fun logout(): Completable

}