package com.texode.plg.domain.usecase.login

import com.texode.plg.domain.base.executor.PostExecutionThread
import com.texode.plg.domain.base.executor.ThreadExecutor
import com.texode.plg.domain.base.interactor.CompletableUseCase
import com.texode.plg.domain.repository.AuthRepository
import io.reactivex.Completable
import javax.inject.Inject

open class LogoutUser @Inject constructor(
		private val authRepository: AuthRepository,
		threadExecutor: ThreadExecutor,
		postExecutionThread: PostExecutionThread) : CompletableUseCase<Nothing?>(threadExecutor, postExecutionThread) {

	override fun buildUseCaseObservable(params: Nothing?): Completable {
		return authRepository.logout()
	}

}