package com.texode.plg.domain.model.login

/**
 * Information used to authenticate user
 */
data class LoginParams(

		val email: String,

		val password: String

)