package com.texode.plg.domain.model

data class Customer(

		val id: Long?,

		val name: String?

)