package com.texode.plg.domain.model

data class Warehouse(

		val id: Long?,

		val name: String?

)