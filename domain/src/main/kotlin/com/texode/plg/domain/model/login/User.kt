package com.texode.plg.domain.model.login

import org.threeten.bp.LocalDateTime

/**
 * Information about currently authorized user
 */
data class User(

		val id: Long?,

		val name: String?,

		val email: String?,

		val refreshToken: String?,

		val createdAt: LocalDateTime?,

		val updatedAt: LocalDateTime?,

		val roleId: Long?,

		val tokens: List<UserToken?>?,

		val role: UserRole?

)