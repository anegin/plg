package com.texode.plg.domain.model.login

/**
 * Information about currently authorized user and access token
 */
data class LoginInfo(

		val user: User?,

		val accessToken: String?,

		val expiresIn: Long?,

		val tokenType: String?

)