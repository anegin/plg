package com.texode.plg.domain.model.login

import org.threeten.bp.LocalDateTime

data class UserRole(

		val id: Long?,

		val tag: String?,

		val name: String?,

		val scopes: List<String?>?,

		val createdAt: LocalDateTime?,

		val updatedAt: LocalDateTime?

)
