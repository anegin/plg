package com.texode.plg.domain.model.login

import org.threeten.bp.LocalDateTime

data class UserToken(

		val id: String?,

		val userId: Long?,

		val clientId: Long?,

		val name: String?,

		val scopes: List<String?>?,

		val revoked: Boolean?,

		val createdAt: LocalDateTime?,

		val updatedAt: LocalDateTime?,

		val expiresAt: LocalDateTime?

)