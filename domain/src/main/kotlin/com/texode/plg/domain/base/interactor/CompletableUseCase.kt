package com.texode.plg.domain.base.interactor

import com.texode.plg.domain.base.executor.PostExecutionThread
import com.texode.plg.domain.base.executor.ThreadExecutor
import io.reactivex.Completable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.observers.DisposableCompletableObserver
import io.reactivex.schedulers.Schedulers

/**
 * Abstract class for a UseCase that returns an instance of a [Completable].
 */
abstract class CompletableUseCase<in Params> protected constructor(
		private val threadExecutor: ThreadExecutor,
		private val postExecutionThread: PostExecutionThread) {

	private val disposables = CompositeDisposable()

	/**
	 * Builds a [Completable] which will be used when the current [CompletableUseCase] is executed.
	 */
	protected abstract fun buildUseCaseObservable(params: Params? = null): Completable

	/**
	 * Executes the current use case.
	 */
	open fun execute(completableObserver: DisposableCompletableObserver, params: Params? = null) {
		val completable = this.buildUseCaseObservable(params)
				.subscribeOn(Schedulers.from(threadExecutor))
				.observeOn(postExecutionThread.scheduler) as Completable
		disposables.add(completable.subscribeWith(completableObserver))
	}

	/**
	 * Unsubscribes from current [Disposable].
	 */
	fun unsubscribe() {
		if (disposables.size() > 0) {
			disposables.clear()
		}
	}

}